import Component from "../components/Component.js";
import Router from "../Router.js";
import PizzaThumbnail from "./../components/PizzaThumbnail.js";
export default class PizzaList extends Component {
	#pizzas;
	constructor(data) {
		super("section", { name: "class", value: "pizzaList" }, data);
	}
	set pizzas(value){
		this.#pizzas = value;
		Router.routes.page = this;
		Router.titleElement = document.querySelector(".pageTitle");
		Router.contentElement = document.querySelector(".pageContent");
	}
	renderChildren() {
		let html = "";
		this.children.forEach((element) => {
			html += new PizzaThumbnail(element).render();
		});
		return html;
	}
}
