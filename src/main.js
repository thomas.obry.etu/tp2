import Component from "./components/Component.js";
import Img from "./components/Img.js";
import PizzaThumbnail from "./components/PizzaThumbnail.js";
import PizzaList from "./pages/PizzaList.js";
import data from "./data.js";
import Router from "./Router.js";

const c = new Component("article", { name: "class", value: "pizzaThumbnail" }, [
	new Img(
		"https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300"
	),
	"Regina",
]);

// const title = new Component("h1", null, ["La", " ", "carte"]);
// document.querySelector(".pageTitle").innerHTML = title.render();

// document.querySelector(".pageContent").innerHTML = pizzaList.render();
// console.log("pizzalist = " + pizzaList.render());

const pizzaList = new PizzaList(data);
Router.titleElement = document.querySelector(".pageTitle");
Router.contentElement = document.querySelector(".pageContent");
Router.routes = { path: "/", page: pizzaList, title: new Component("h1", null, ["La", " ", "carte"]) };
Router.navigate("/");


// const pizzaList = new PizzaList([]); 
// Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];
// Router.navigate('/'); // affiche une page vide
// pizzaList.pizzas = data;
// Router.navigate('/'); // affiche la liste des pizzas