import Component from "./components/Component.js";
import Img from "./components/Img.js";
import PizzaThumbnail from "./components/PizzaThumbnail.js";
import PizzaList from "./pages/PizzaList.js";
import data from "./data.js";

export default class Router {
	static titleElement;
	static contentElement;
	static routes;

	static navigate(path) {
		this.titleElement.innerHTML = this.routes.title.render();
		this.contentElement.innerHTML = this.routes.page.render();
	}
}

/*

const title = new Component("h1", null, ["La", " ", "carte"]);
document.querySelector(".pageTitle").innerHTML = title.render();

const pizzaList = new PizzaList(data);
document.querySelector(".pageContent").innerHTML = pizzaList.render();
console.log("pizzalist = " + pizzaList.render());

Router.routes = [{ path: "/", page: pizzaList, title: "La carte" }];

*/