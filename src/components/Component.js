export default class Component {
	constructor(tagname, attribute, children) {
		this.tagname = tagname;
		this.attribute = attribute;
		this.children = children;
	}
	render() {
		if (this.children)
			return `<${
				this.tagname
			} ${this.renderAttribute()}>${this.renderChildren()}</${this.tagname}>`;
		else return `<${this.tagname} ${this.renderAttribute()}"/>`;
	}
	renderAttribute() {
		if (this.attribute)
			return `${this.attribute.name}="${this.attribute.value}"`;
	}
	renderChildren() {
		if (this.children instanceof Array) {
			let string = "";
			this.children.forEach((element) => {
				string += this.renderChild(element);
			});
			return string;
		} else return this.renderChild(this.children);
	}
	renderChild(element) {
		if (element instanceof Component) return element.render();
		else return element;
	}
}